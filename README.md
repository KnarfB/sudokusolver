# SudokuSolver

Building from the command line or in your favorite IDE should be a no-brainer, tested with
* gcc (Ubuntu 9.4.0-1ubuntu1~20.04.1) 9.4.0: `$ gcc -std=c18 -pedantic -Wall -o sudoku sudoku.c`
* clang version 13.0.0:  `clang.exe -std=c99 -pedantic -Wall -o sudoku.exe sudoku.c`

Uncomment logging output in solve to watch backtracking

Video: https://mediathek.htw-berlin.de/video/CE-C21-ADK-Sudoku-Solver-in-C/01954d6d81bd3cdc6686766e4c9d0c1e

A Sudoku generator: https://github.com/Periculum/SudokuGenerator/blob/main/SudokuGenerator.py
