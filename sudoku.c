// Sudoku Solver 0.2 - Frank Bauernoeppel

#include <stdio.h>
#include <stdlib.h>

#define N 3

#define NN (N*N)

int S0[NN][NN];

int S1[NN][NN] = {
    {  0, 0, 3,  0, 0, 6,  0, 0, 0 },
    {  1, 4, 0,  8, 0, 0,  0, 0, 0 },
    {  0, 2, 0,  9, 0, 0,  0, 0, 0 },

    {  0, 8, 4,  0, 0, 0,  7, 0, 0 },
    {  9, 6, 0,  0, 0, 7,  0, 8, 0 },
    {  0, 0, 0,  4, 0, 0,  2, 0, 9 },

    {  0, 3, 0,  0, 0, 9,  8, 1, 0 },
    {  0, 0, 0,  0, 7, 3,  9, 0, 6 },
    {  0, 0, 0,  0, 0, 0,  4, 7, 0 }
};

int S2[NN][NN] = {
    {  5, 3, 0,  0, 7, 0,  0, 0, 0 },
    {  6, 0, 0,  1, 9, 5,  0, 0, 0 },
    {  0, 9, 8,  0, 0, 0,  0, 6, 0 },

    {  8, 0, 0,  0, 6, 0,  0, 0, 3 },
    {  4, 0, 0,  8, 0, 3,  0, 0, 1 },
    {  7, 0, 0,  0, 2, 0,  0, 0, 6 },

    {  0, 6, 0,  0, 0, 0,  2, 8, 0 },
    {  0, 0, 0,  4, 1, 9,  0, 0, 5 },
    {  0, 0, 0,  0, 8, 0,  0, 7, 9 }
};

void printS(int S[NN][NN])
{
    for (int i = 0; i < NN; ++i) {
        if ((i % N) == 0) {
            printf("+-----------------------------+\n");
        }
        for (int j = 0; j < NN; ++j) {
            if ((j % N) == 0) {
                printf("|");
            }
            printf(" %c ", S[i][j] ? '0' + S[i][j] : '.');
        }
        printf("|\n");
    }
    printf("+-----------------------------+\n");
}

// check if one area (1xNN, NNx1 or NxN) is correctly filled
int conflictOne(int S[NN][NN], int r0, int c0, int h, int w)
{
    int histogram[NN+1];
    for (int i = 0; i < NN+1; ++i) {
        histogram[i] = 0;
    }

    for (int rr = r0; rr < r0 + h; ++rr) {
        for (int cc = c0; cc < c0 + w; ++cc) {
            int x = S[rr][cc];
            if ( (x > 0) && (histogram[x] > 0) ) {
                //printf("conflict with (%d,%d)==%d\n", rr, cc, x);
                return 1;
            }
            histogram[x]++; // value x used in row/col/block
        }
    }
    return 0; // no conflict
}

// does the sudoku contain any conflict ?
int conflictAny(int S[NN][NN])
{
    for (int r = 0; r < NN; ++r) {
        if (conflictOne(S, r, 0, 1, NN)) {
            return 1;
        }
    }

    for (int c = 0; c < NN; ++c) {
        if (conflictOne(S, 0, c, NN, 1)) {
            return 1;
        }
    }

    for (int r = 0; r < NN; r += N) {
        for (int c = 0; c < NN; c += N) {
            if (conflictOne(S, r, c, N, N)) {
                return 1;
            }
        }
    }

    return 0;
}

int solve(int S[NN][NN], int r, int c)
{
    // system("cls"); // Windows specific command: clear screen
    // printf("\033[2J\033[1;1H"); // ANSI terminal specific control code: cursor home
    // printS(S); // print partial solution
    // printf("solving cell (%d,%d)\n", r, c);

    if (r == NN) {
        return 1;   // entire sudoku filled
    }

    // find next index pair [rr][cc]
    int rr = r;
    int cc = c + 1;
    if (cc == NN) {
        cc = 0;
        rr = r + 1;
    }

    if (S[r][c] != 0) {
        return solve(S, rr, cc);
    } else {
        for (int x = 1; x <= NN; ++x) {
            S[r][c] = x; // try x
            if (!conflictAny(S)) {
                if (solve(S, rr, cc)) {
                    return 1;
                }
            }
        }
        //printf("backtracking\n");
        S[r][c] = 0;
        return 0;
    }
}

int main()
{
    printS(S1);
    if (solve(S1, 0, 0)) {
        printf("solution found:\n");
        printS(S1);
    }
    else {
        printf("no solution found\n");
    }
    return 0;
}
